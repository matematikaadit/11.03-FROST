# FROST

FORTH-like language written in Rust.

# License

MIT License, see [LICENSE](LICENSE) for details.
