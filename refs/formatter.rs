use std::io::{self, BufRead};
use std::fmt;
use std::env;

#[derive(Copy, Clone, Debug, Ord, Eq, PartialOrd, PartialEq)]
enum Set {
    Core,
    CoreExt,
    Block,
    BlockExt,
    Double,
    DoubleExt,
    Exception,
    ExceptionExt,
    Facility,
    FacilityExt,
    File,
    FileExt,
    Floating,
    FloatingExt,
    Local,
    LocalExt,
    Memory,
    Tools,
    ToolsExt,
    Search,
    SearchExt,
    String,
}

impl Set {
    fn new(s: String) -> Set {
        use Set::*;
        match &s[..] {
            "CORE"          => Core,
            "CORE EXT"      => CoreExt,
            "BLOCK"         => Block,
            "BLOCK EXT"     => BlockExt,
            "DOUBLE"        => Double,
            "DOUBLE EXT"    => DoubleExt,
            "EXCEPTION"     => Exception,
            "EXCEPTION EXT" => ExceptionExt,
            "FACILITY"      => Facility,
            "FACILITY EXT"  => FacilityExt,
            "FILE"          => File,
            "FILE EXT"      => FileExt,
            "FLOATING"      => Floating,
            "FLOATING EXT"  => FloatingExt,
            "LOCAL"         => Local,
            "LOCAL EXT"     => LocalExt,
            "MEMORY"        => Memory,
            "TOOLS"         => Tools,
            "TOOLS EXT"     => ToolsExt,
            "SEARCH"        => Search,
            "SEARCH EXT"    => SearchExt,
            "STRING"        => String,
            _               => unimplemented!(),
        }
    }
}

impl fmt::Display for Set {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use Set::*;
        let s = match self {
            Core         => "CORE",
            CoreExt      => "CORE EXT",
            Block        => "BLOCK",
            BlockExt     => "BLOCK EXT",
            Double       => "DOUBLE",
            DoubleExt    => "DOUBLE EXT",
            Exception    => "EXCEPTION",
            ExceptionExt => "EXCEPTION EXT",
            Facility     => "FACILITY",
            FacilityExt  => "FACILITY EXT",
            File         => "FILE",
            FileExt      => "FILE EXT",
            Floating     => "FLOATING",
            FloatingExt  => "FLOATING EXT",
            Local        => "LOCAL",
            LocalExt     => "LOCAL EXT",
            Memory       => "MEMORY",
            Tools        => "TOOLS",
            ToolsExt     => "TOOLS EXT",
            Search       => "SEARCH",
            SearchExt    => "SEARCH EXT",
            String       => "STRING",
        };
        f.pad(s)
    }
}

struct Escape(String);

impl fmt::Display for Escape {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut out = String::new();
        let bytes = self.0.as_bytes();
        let mut start_index = 0;
        for i in 0..bytes.len() {
            let trans = match bytes[i] {
                b'<'  => Some("&lt;"),
                b'>'  => Some("&gt;"),
                b'&'  => Some("&amp;"),
                b'"'  => Some("&quot;"),
                b'\'' => Some("&#39;"),
                _     => None,
            };
            if let Some(s) = trans {
                out.push_str(&self.0[start_index..i]);
                out.push_str(s);
                start_index = i+1;
            }
        }
        if start_index != bytes.len() {
            out.push_str(&self.0[start_index..])
        }
        f.pad(&out)
    }
}

fn make(s: &str, start: usize, end: usize) -> String {
    String::from(s[start..end].trim_end())
}

fn numfmt(section: u8, index: u8) -> String {
    format!("{}.{:03}", section as char, index)
}

fn main() {
    let mut args = env::args();
    args.next(); // ignore program name
    let text_mode = args.next() == Some("--text".to_string());

    let stdin = io::stdin();
    let mut lines = stdin.lock().lines();
    let mut vec = Vec::new();
    while let Some(Ok(line)) = lines.next() {
        let name = make(&line, 0, 16);
        let xt = make(&line, 16, 39);
        let xt = if xt.len() == 0 { name.to_ascii_lowercase() } else { xt };
        let set = Set::new(make(&line, 39, line.len()));

        vec.push((set, name, xt));
    }

    vec.sort();
    let mut index = 1;
    let mut section = b'A';
    let mut prev = vec[0].0;
    for (set, name, xt) in vec {
        if prev != set {
            index = 1;
            section += 1;
        }
        if text_mode {
            println!("{: >14} , {: >19} , {}", set, xt, name);
        } else {
            println!("<tr><td>{: >5}</td><td>{: >14}</td><td><b>{: >19}</b></td><td> {}</td></tr>", numfmt(section, index), set, Escape(xt), Escape(name));
        }
        prev = set;
        index += 1;
    }
}
